package cat.itb.CRUDS;

import cat.itb.EmpleadosEntity;
import cat.itb.Main;
import org.hibernate.*;

public class Empleados_CRUD
{
    public void insert(EmpleadosEntity employee)
    {
        Session currentSession = Main.getSession();
        Transaction tx = currentSession.beginTransaction();

        currentSession.save(employee);

        tx.commit();

        System.out.println("Successfully Saved");
    }

    public void updateSalary(short empNo, double salary)
    {
        Session currentSession = Main.getSession();
        Transaction tx = currentSession.beginTransaction();

        EmpleadosEntity employee = currentSession.load(EmpleadosEntity.class, empNo);

        employee.setSalario(salary);

        currentSession.update(employee);

        tx.commit();
    }

    public void deleteEmployee(String name)
    {
        Session currentSession = Main.getSession();
        Transaction tx = currentSession.beginTransaction();

        Query query = currentSession.createQuery("delete EmpleadosEntity where apellido = :name");
        query.setParameter("name", name);

        query.executeUpdate();

        tx.commit();
    }

    public void employeeWhereSalaryGreater(double salary)
    {
        Query query = Main.getSession().createQuery("from EmpleadosEntity where salario > :salary");
        query.setParameter("salary", salary);

        for (final Object o : query.list()) System.out.println(o);
    }
}

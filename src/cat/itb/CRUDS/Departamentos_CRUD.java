package cat.itb.CRUDS;

import cat.itb.DepartamentosEntity;
import cat.itb.Main;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class Departamentos_CRUD
{
    public void insert(DepartamentosEntity department)
    {
        Session currentSession = Main.getSession();
        Transaction tx = currentSession.beginTransaction();

        currentSession.save(department);

        tx.commit();

        System.out.println("Successfully Saved");
    }


    public void updateName(byte empNo, String name)
    {
        Session currentSession = Main.getSession();
        Transaction tx = currentSession.beginTransaction();

        DepartamentosEntity department = currentSession.load(DepartamentosEntity.class, empNo);

        department.setDnombre(name);

        currentSession.update(department);

        tx.commit();
    }
}

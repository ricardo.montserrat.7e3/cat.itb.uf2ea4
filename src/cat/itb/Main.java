package cat.itb;

import cat.itb.CRUDS.Departamentos_CRUD;
import cat.itb.CRUDS.Empleados_CRUD;
import cat.itb.Utilities.Tools;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.sql.Date;

public class Main implements Tools.Menu
{
    private static final SessionFactory ourSessionFactory;

    static
    {
        try
        {
            Configuration configuration = new Configuration();
            configuration.configure();

            ourSessionFactory = configuration.buildSessionFactory();
        } catch (Throwable ex) { throw new ExceptionInInitializerError(ex); }
    }

    public static Session getSession() throws HibernateException
    {
        return ourSessionFactory.openSession();
    }

    public boolean getInputMenu(String input)
    {
        switch (input)
        {
            case "1" -> {
                Departamentos_CRUD departamentos_crud = new Departamentos_CRUD();
                departamentos_crud.insert(new DepartamentosEntity((byte) 60, "TECNOLOGIA", "BARCELONA"));
                departamentos_crud.insert(new DepartamentosEntity((byte) 70, "INFORMATICA", "SEVILLA"));
            }
            case "2" -> new Empleados_CRUD().insert(new EmpleadosEntity((short) 8000, "Montserrat", "Developer", (short)25, new Date(2000,1,31), 500.0, 0.0, new DepartamentosEntity((byte) 60, "TECNOLOGIA", "BARCELONA")));
            case "3" -> new Departamentos_CRUD().updateName((byte) 20, "RECERCA");
            case "4" -> new Empleados_CRUD().updateSalary((short) 7499, 2100.00);
            case "5" -> new Empleados_CRUD().deleteEmployee("SALA");
            case "6" -> new Empleados_CRUD().employeeWhereSalaryGreater(2000);
            case "7" -> { return true; }
        }
        return false;
    }

    public void showMenu()
    {
        System.out.println("""
                ---------- Welcome To My Program ----------
                
                1.- Insert Departments.
                2.- Insert Employee.
                3.- Update Department 20 Name.
                4.- Update Salary Employee 7499.
                5.- Delete Employee "SALA".
                6.- See Employees Salary Greater than 2000.
                7.- Exit.
                
                """);
    }

    public static void main(final String[] args)
    {
        Main program = new Main();

        program.menu("Select and option by number: ");
    }
}